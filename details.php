<?php
include('config/db_connect.php');

// post method for deleting a pizza from database
if(isset($_POST['delete'])){
    $id_to_delete = mysqli_real_escape_string($conn, $_POST['id_to_delete']);
    $sql = "DELETE FROM PIZZA WHERE id = $id_to_delete";
    if(mysqli_query($conn, $sql)){
        //if delete is successful, the user will be directed to index page
        header('Location: index.php');
    } {
        echo 'query error' . mysqli_error($conn);
    }
}
$piza = '';
// check GET requestid param


if(isset($_GET['id'])){
    $id = mysqli_real_escape_string($conn, $_GET['id']);

    //make sql query to select only one data based on the id
    $sql = "SELECT * FROM pizza WHERE id = $id";
   


    //get the query result
    $result = mysqli_query($conn, $sql) or die( mysqli_error($conn));;

    //fetch result in array format
    $piza = mysqli_fetch_assoc($result);

    mysqli_free_result($result);
    mysqli_close($conn);
}


?>



<!DOCTYPE html>
<html>
<?php include('template/header.php');?>

<div class ="container center">
    <?php if($piza): ?>

        <h4><?php echo htmlspecialchars($piza['title']); ?></h4>
        <p>Created by: <?php echo htmlspecialchars($piza['email']); ?></p>
        <p><?php echo date($piza['created_at']); ?></p>>
        <h5>Ingredients:</h5>
        <p><?php echo htmlspecialchars($piza['ingredients']); ?></p>
    
        <!-- DELETING A PIZZA FROM DATABASE -->
        <form action="details.php" method="POST">
            <input type="hidden" name="id_to_delete" value="<?php echo $piza['id'] ?>">
            <input type="submit" name="delete" value="Delete" class="btn brand z-depth-0">
        </form>
    
    
    <?php else: ?>
        <h5> No such Pizza exist</h5>
    <?php endif; ?>
</div>

<?php include('template/footer.php');?>

</html>