<?php

include('config/db_connect.php');

//GETTING DATA FROM THE DATABASE

//1. Write a query to get all product
$sql = 'SELECT title, ingredients, id FROM pizza ORDER BY created_at';

//2. make a query and get result
$result = mysqli_query($conn, $sql);

//3. fetch the resulting row as an array
$pizza =mysqli_fetch_all($result, MYSQLI_ASSOC);

//4. free results from memory
mysqli_free_result($result);

//5. close connection
mysqli_close($conn);
//print_r($pizza);

?>



<!DOCTYPE html>
<html>
    <?php include('template/header.php');?>

<!-- Rendering data from the database to the browser -->

<h4 class = "center grey-text">Pizzas!</h4>
<div class = "container">
    <div class = "row">

        <?php foreach($pizza as $piza): ?>
            <div class = "col s6 md3">
                <div class = "card z-depth-0">
                    <img src="img/bbq-pizza.jpg" class="pizza">
                    <div class = "card-content center">
                        <h6><?php echo htmlspecialchars($piza['title']); ?></h6>
                        <!-- //using the explode fuction to make a list of string into array -->
                        <ul>
                            <?php foreach(explode(',', $piza['ingredients']) as $ing): ?>
                                <li><?php echo htmlspecialchars($ing); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div calss = "card-action right-align">
                        <a class="brand-text" href="details.php?id=<?php echo $piza['id']?>">More Info</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>

    <?php include('template/footer.php');?>

</html>